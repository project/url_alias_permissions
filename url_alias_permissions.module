<?php

/**
 * @file
 * Contains url_alias_permissions.module.
 */

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Implements hook_help().
 */
function url_alias_permissions_help(string $route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the Field Permissions module.
    case 'help.page.url_alias_permissions':
      return '<p>' . t('Grant access to users to edit and create path aliases for each entity type.') . '</p>';
  }
}

/**
 * Implements hook_entity_field_access().
 */
function url_alias_permissions_entity_field_access($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, FieldItemListInterface $items = NULL) {
  if ($operation !== 'edit' || $field_definition->getType() !== 'path') {
    return AccessResult::neutral();
  }

  $entity = $items->getEntity();
  $entity_type_id = $entity->getEntityTypeId();
  $bundle_id = $entity->bundle();
  $entity_type = $entity->getEntityType();

  $permission = NULL;
  switch ($entity_type->getPermissionGranularity()) {
    case 'bundle':
      $permission = "edit $bundle_id $entity_type_id url alias";
      break;

    case 'entity_type':
      $permission = "edit $entity_type_id url alias";
      break;
  }

  if ($permission === NULL) {
    return AccessResult::neutral();
  }

  return AccessResult::allowedIfHasPermissions($account, [
    $permission,
    'create url aliases',
    'administer url aliases',
  ], 'OR')->cachePerPermissions();
}
